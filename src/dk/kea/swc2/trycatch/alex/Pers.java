/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.trycatch.alex;

/**
 *
 * @author alex
 */
public class Pers {

    private int age;
    private String name;

    public Pers(int age, String name) throws Exception {
        if (age > 0 && age < 100) {
            this.age = age;
        } else {
            throw new Exception("The age is invalid");
        }
        this.name = name;
    }

    @Override
    public String toString() {
        return "Pers{" + "age=" + age + ", name=" + name + '}';
    }
    
    

}
