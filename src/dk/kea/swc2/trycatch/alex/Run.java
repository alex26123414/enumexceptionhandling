package dk.kea.swc2.trycatch.alex;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
public class Run {

    public static void main(String[] args) {

        try {
            //Run.myMethod();
            System.out.println("Instruction in try.");
            Pers p3 = new Pers(-3, "Bob");
        } catch (MyException ex) {
            System.err.println(ex);
            //System.out.println("Hi from catch.");

        } catch (Exception ex) {
            System.err.println(ex);
        } finally {
            System.out.println("This is the finally instruction.");
        }
        
        System.out.println("This is outside the try and catch block.");
        /*
         try {
         Pers p1 = new Pers(25, "Alex");
         System.out.println(p1);
         Pers p2 = new Pers(0, "Alex");
         System.out.println(p2);
         } catch (Exception ex) {
         System.err.println("The age was invalid, so I created a default object for you. You're welcome!");
         try {
         Pers p = new Pers(1, "This is not a name");
         System.out.println(p);
         } catch (Exception ex1) {
         Logger.getLogger(Run.class.getName()).log(Level.SEVERE, null, ex1);
         }
         }
         */
        /*
         Scandinavia place = Scandinavia.Denmark;
         String placeString = Scandinavia.Denmark.toString();
       
         for (Scandinavia country : Scandinavia.values()) {
         System.out.println(country);
         }
        
         System.out.println("There are " + place.noOfPopulation() + " inside " + place);
        
         */

        /*
         String myStrangeString = +1 + - -+- -+ + + + +- 1 + " ";
         System.out.println("|" + myStrangeString + "|");

         double d = 0;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
         d += 0.1;
         System.out.println(d);
        
         System.out.println(0.0/0.0);
         System.out.println(-3/0);
         */
    }

    public static void myMethod() throws MyException {
        throw new MyException("Somthing is wrrong");
    }
}
