package dk.kea.swc2.trycatch.alex;

/**
 *
 * @author alex
 */
public class MyException extends Exception{

    public MyException(String message) {
        super(message);
    }
    
    
}
