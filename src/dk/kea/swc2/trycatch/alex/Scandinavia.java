package dk.kea.swc2.trycatch.alex;

/**
 *
 * @author alex
 */
public enum Scandinavia {

    Denmark, Sweden, Norway, Finland;

    public String noOfPopulation() {
        switch (this) {
            case Denmark:
                return "5.6 m";
            case Sweden:
                return "8 m";
            case Norway:
                return "5 m";
            case Finland:
                return "5.4 m";
            default:
                return "This is not a Scandinavian";
        }
    }
}
